package com.rockitgroup.fantasylotto.domain.service.account;

import com.rockitgroup.fantasylotto.domain.model.account.AccountActivityTracking;
import com.rockitgroup.fantasylotto.domain.repository.account.AccountActivityTrackingRepository;
import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class AccountActivityTrackingService extends DefaultBaseService {

    @Autowired
    private AccountActivityTrackingRepository accountActivityTrackingRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = accountActivityTrackingRepository;
    }

    @Async
    public void saveAsync(AccountActivityTracking accountActivityTracking) {
        accountActivityTrackingRepository.saveAndFlush(accountActivityTracking);
    }
}
