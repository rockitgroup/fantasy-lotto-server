package com.rockitgroup.fantasylotto.domain.model.enumeration;

public enum LottoTicketPurchaseRequestStatusEnum {
    PENDING, PURCHASED
}
