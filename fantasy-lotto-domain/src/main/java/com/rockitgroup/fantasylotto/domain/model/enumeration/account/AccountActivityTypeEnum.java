package com.rockitgroup.fantasylotto.domain.model.enumeration.account;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 11/4/18
 * Time: 3:13 PM
 */
public enum AccountActivityTypeEnum {
    REGISTER_ACCOUNT_DEVICE,
    LINK_SOCIAL,
    LOGIN_EMAIL,
    CONFIRM_LOGIN_EMAIL
}
