package com.rockitgroup.fantasylotto.domain.model.enumeration;

public enum  LottoBetStatusEnum {

    PENDING, ACTIVE
}
