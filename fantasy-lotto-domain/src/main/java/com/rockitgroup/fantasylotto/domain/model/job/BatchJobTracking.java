package com.rockitgroup.fantasylotto.domain.model.job;

import com.rockitgroup.fantasylotto.domain.model.enumeration.TrackingEventResultEnum;
import com.rockitgroup.fantasylotto.domain.model.enumeration.TrackingEventStatusEnum;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
@Entity
public class BatchJobTracking extends Modifiable {

    private String jobName;

    @Column(length = 65535,columnDefinition="Text")
    @Type(type="text")
    private String output;

    @Enumerated(EnumType.STRING)
    private TrackingEventStatusEnum status;

    @Enumerated(EnumType.STRING)
    private TrackingEventResultEnum result;

    public static BatchJobTracking create(String jobName) {
        BatchJobTracking batchJobTracking = new BatchJobTracking();
        batchJobTracking.setJobName(jobName);
        batchJobTracking.setStatus(TrackingEventStatusEnum.IN_PROGRESS);
        return batchJobTracking;
    }


}
