package com.rockitgroup.fantasylotto.domain.repository.game;

import com.rockitgroup.fantasylotto.domain.model.enumeration.account.GameSystemTypeEnum;
import com.rockitgroup.fantasylotto.domain.model.game.GameSystemSetting;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameSystemSettingRepository extends BaseRepository<GameSystemSetting, Long> {

    List<GameSystemSetting> findAllByGameIdAndSystemType(Long gameId, GameSystemTypeEnum systemType);

    GameSystemSetting findOneByGameIdAndSystemTypeAndSettingKey(Long gameId, GameSystemTypeEnum systemType, String settingKey);
}
