package com.rockitgroup.fantasylotto.domain.model.lotto.ticket;

import com.rockitgroup.fantasylotto.domain.model.lotto.game.LottoGame;
import com.rockitgroup.infrastructure.vitamin.common.model.Deletable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class LottoTicket extends Deletable {

    @OneToOne
    private LottoTicketPurchaseRequest purchaseRequest;

    @OneToOne
    private LottoGame lottoGame;

    private Double price;

    private String scannedTicketImageUrl;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lottoTicket", fetch = FetchType.LAZY)
    private List<LottoTicketBall> balls;
}
