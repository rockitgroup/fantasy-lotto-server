package com.rockitgroup.fantasylotto.domain.model.game;


import com.rockitgroup.fantasylotto.domain.model.account.Account;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class AccountGameSetting extends Modifiable {

    @ManyToOne
    @JoinColumn
    private Account account;

    @OneToOne
    private Game game;

    private String settingKey;
    private String settingValue;

    public static AccountGameSetting create(Account account, Game game, String settingKey, String settingValue) {
        AccountGameSetting accountGameSetting = new AccountGameSetting();
        accountGameSetting.setAccount(account);
        accountGameSetting.setGame(game);
        accountGameSetting.setSettingKey(settingKey);
        accountGameSetting.setSettingValue(settingValue);
        return accountGameSetting;
    }
}
