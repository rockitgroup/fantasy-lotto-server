package com.rockitgroup.fantasylotto.domain.repository.game;

import com.rockitgroup.fantasylotto.domain.model.game.AccountGameSetting;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountGameSettingRepository extends BaseRepository<AccountGameSetting, Long> {

    List<AccountGameSetting> findAllByAccountIdAndGameIdAndSettingKey(Long accountId, Long gameId, String settingKey);
}
