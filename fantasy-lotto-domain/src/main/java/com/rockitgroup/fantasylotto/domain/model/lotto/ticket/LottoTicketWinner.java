package com.rockitgroup.fantasylotto.domain.model.lotto.ticket;

import com.rockitgroup.fantasylotto.domain.model.enumeration.LottoBetWinnerStatusEnum;
import com.rockitgroup.fantasylotto.domain.model.lotto.game.LottoGameResult;
import com.rockitgroup.infrastructure.vitamin.common.model.Deletable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

@Getter
@Setter
@Entity
public class LottoTicketWinner extends Deletable {

    @OneToOne
    private LottoTicket ticket;

    @OneToOne
    private LottoGameResult result;

    private Double winningAmount;

    @Enumerated(EnumType.STRING)
    private LottoBetWinnerStatusEnum status;

}
