package com.rockitgroup.fantasylotto.domain.model.enumeration.account;

public enum GameSystemSettingKeyEnum {

    EMAIL_SUBJECT("emailSubject");

    private String value;

    GameSystemSettingKeyEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
