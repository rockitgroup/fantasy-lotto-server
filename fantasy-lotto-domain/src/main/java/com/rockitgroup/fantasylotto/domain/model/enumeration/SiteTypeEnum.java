package com.rockitgroup.fantasylotto.domain.model.enumeration;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 2019-04-27
 * Time: 02:24
 */
public enum SiteTypeEnum {

    APP,
    ARTICLE
}
