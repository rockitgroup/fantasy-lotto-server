package com.rockitgroup.fantasylotto.domain.model.lotto.bet;

import com.rockitgroup.fantasylotto.domain.model.enumeration.LottoBetModeTypeEnum;
import com.rockitgroup.infrastructure.vitamin.common.model.Deletable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
@Entity
public class LottoBetMode extends Deletable {

    @Enumerated(EnumType.STRING)
    private LottoBetModeTypeEnum type;

    private String name;
    private String description;

    private Integer winningRate;                // If 60, it mean put 1 dollar will win 60 dollar
    private Integer totalCoverBallAllow;        // Bao lô, số lượng ball đc bao lô

}
