package com.rockitgroup.fantasylotto.domain.dto.account.request;

import com.rockitgroup.infrastructure.vitamin.common.dto.RequestDTO;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.Assert;

@Getter
@Setter
public class UpdateAccountEmailRequestDTO extends RequestDTO {

    private String email;

    @Override
    public void validate() throws IllegalArgumentException {
        Assert.notNull(email, "Missing parameter email");
    }
}
