package com.rockitgroup.fantasylotto.domain.service.account;

import com.rockitgroup.fantasylotto.domain.repository.account.GroupRepository;
import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class GroupService extends DefaultBaseService {

    @Autowired
    private GroupRepository groupRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = groupRepository;
    }

}
