package com.rockitgroup.fantasylotto.domain.dto.game;

import com.rockitgroup.infrastructure.vitamin.common.dto.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountGameSettingDTO extends BaseDTO {

    private String settingKey;
    private String settingValue;
}
