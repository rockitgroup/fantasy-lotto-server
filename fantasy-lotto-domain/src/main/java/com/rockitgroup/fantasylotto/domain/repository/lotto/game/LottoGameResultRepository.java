package com.rockitgroup.fantasylotto.domain.repository.lotto.game;

import com.rockitgroup.fantasylotto.domain.model.lotto.game.LottoGameResult;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LottoGameResultRepository extends BaseRepository<LottoGameResult, Long> {


}

