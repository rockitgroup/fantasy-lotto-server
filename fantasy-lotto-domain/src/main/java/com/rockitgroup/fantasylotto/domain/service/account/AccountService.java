package com.rockitgroup.fantasylotto.domain.service.account;

import com.rockitgroup.fantasylotto.domain.model.account.Account;
import com.rockitgroup.fantasylotto.domain.model.account.AccountAccessIssue;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccessIssueConfirmTypeEnum;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccessIssueTypeEnum;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountStatusEnum;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.GameSystemTypeEnum;
import com.rockitgroup.fantasylotto.domain.model.game.Game;
import com.rockitgroup.fantasylotto.domain.repository.account.AccountRepository;
import com.rockitgroup.fantasylotto.domain.service.cache.AccountCacheService;
import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class AccountService extends DefaultBaseService {

    private static final String UPDATE_ACCOUNT_EMAIL_CONFIRMATION_EMAIL_TEMPLATE = "update-account-email-confirmation-template.ftl";
    private static final String LOGIN_EMAIL_CONFIRMATION_EMAIL_TEMPLATE = "login-email-confirmation-template.ftl";

    @Autowired
    private AccountAccessIssueService accountAccessIssueService;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountCacheService accountCacheService;

    @PostConstruct
    public void postConstruct() {
        this.repository = accountRepository;
    }


    public Account findAccountById(Long accountId) {
        Optional<Account> account =  accountRepository.findById(accountId);
        return account.orElse(null);
    }

    public Account findActiveAccountByEmail(String email) {
        if (email == null) {
            return null;
        }
        return accountRepository.findOneByStatusAndEmail(AccountStatusEnum.ACTIVE, email);
    }

    public AccountAccessIssue confirmAccountAccessIssueEmailType(Account account, Game game, AccessIssueTypeEnum issueType, String confirmCode, String confirmIpAddress) {
        AccountAccessIssue accountAccessIssue = accountAccessIssueService.findActiveIssueByConfirmCode(account.getId(), game.getId(), issueType, AccessIssueConfirmTypeEnum.EMAIL, confirmCode);
        if (accountAccessIssue != null) {
            accountAccessIssueService.confirmIssue(accountAccessIssue, confirmIpAddress);
        }

        return accountAccessIssue;
    }

    public void startUpdateAccountEmailProcess(Account account, Game game, String email) throws Exception {
        accountAccessIssueService.createAccountAccessIssueEmailType(account, game, email, AccessIssueTypeEnum.UPDATE_ACCOUNT_EMAIL, GameSystemTypeEnum.UPDATE_ACCOUNT_EMAIL, UPDATE_ACCOUNT_EMAIL_CONFIRMATION_EMAIL_TEMPLATE);
    }

    public void startLoginEmailProcess(Account account, Game game, String email) throws UnsupportedEncodingException {
        accountAccessIssueService.createAccountAccessIssueEmailType(account, game, email, AccessIssueTypeEnum.LOGIN_EMAIL, GameSystemTypeEnum.LOGIN_EMAIL, LOGIN_EMAIL_CONFIRMATION_EMAIL_TEMPLATE);
    }

    @Override
    protected void clearCache(Object o) {
        Account account = (Account) o;
        accountCacheService.clearAccountCache(account.getId());
    }

    @Override
    protected void clearCaches(List list) {
        for (Object o : list) {
            Account account = (Account) o;
            accountCacheService.clearAccountCache(account.getId());
        }
    }
}
