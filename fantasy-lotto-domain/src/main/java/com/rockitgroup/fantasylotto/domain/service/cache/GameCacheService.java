package com.rockitgroup.fantasylotto.domain.service.cache;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rockitgroup.fantasylotto.domain.dto.game.GameDTO;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountStatusEnum;
import com.rockitgroup.fantasylotto.domain.model.game.Game;
import com.rockitgroup.fantasylotto.domain.repository.game.GameRepository;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.DTOMapper;
import com.rockitgroup.infrastructure.vitamin.common.redis.RedisServer;
import com.rockitgroup.infrastructure.vitamin.common.util.MapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class GameCacheService {

    public final static String GAME_ID_CACHE_KEY = "GAME_ID_%s";

    public final static int GAME_ID_CACHE_TIME = 60 * 60 * 24 * 30; // A month

    public final static String GAME_KEY_CACHE_KEY = "GAME_KEY_%s";

    public final static int GAME_KEY_CACHE_TIME = 60 * 60 * 24 * 30;  // A month

    public final static String GAME_ACCOUNT_CACHE_KEY = "GAME_ACCOUNT_%s";

    public final static int GAME_ACCOUNT_CACHE_TIME = 60 * 60;

    @Autowired
    private RedisServer redisServer;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private DTOMapper dtoMapper;

    private ObjectMapper objectMapper = MapperUtils.getObjectMapper();


    public List<GameDTO> getAccountGames(Long accountId) {
        String cacheKey = String.format(GAME_ACCOUNT_CACHE_KEY, accountId);
        String cacheValue = redisServer.get(cacheKey);
        if (cacheValue == null) {
            setAccountGamesCache(accountId);
            cacheValue = redisServer.get(cacheKey);
        }

        if (cacheValue != null) {
            try {
                return objectMapper.readValue(cacheValue, new TypeReference<List<GameDTO>>() {});
            } catch (Exception e) {
                log.error(String.format("Failed to read value for cache key %s", cacheKey), e);
            }
        }

        return null;
    }

    public GameDTO getGame(String key) {
        String cacheKey = String.format(GAME_KEY_CACHE_KEY, key);
        String cacheValue = redisServer.get(cacheKey);
        if (cacheValue == null) {
            setGameCache(key);
            cacheValue = redisServer.get(cacheKey);
        }

        if (cacheValue != null) {
            try {
                return objectMapper.readValue(cacheValue, GameDTO.class);
            } catch (Exception e) {
                log.error(String.format("Failed to read value for cache key %s", cacheKey), e);
            }
        }

        return null;
    }

    public GameDTO getGame(Long gameId) {
        String cacheKey = String.format(GAME_ID_CACHE_KEY, gameId);
        String cacheValue = redisServer.get(cacheKey);
        if (cacheValue == null) {
            setGameCache(gameId);
            cacheValue = redisServer.get(cacheKey);
        }

        if (cacheValue != null) {
            try {
                return objectMapper.readValue(cacheValue, GameDTO.class);
            } catch (Exception e) {
                log.error(String.format("Failed to read value for cache key %s", cacheKey), e);
            }
        }

        return null;
    }

    public void clearGameCache(String key) {
        String cacheKey = String.format(GAME_KEY_CACHE_KEY, key);
        redisServer.delete(cacheKey);
    }

    public void clearGameCache(Long gameId) {
        String cacheKey = String.format(GAME_ID_CACHE_KEY, gameId);
        redisServer.delete(cacheKey);
    }

    public void clearAccountGamesCache(Long accountId) {
        String cacheKey = String.format(GAME_ACCOUNT_CACHE_KEY, accountId);
        redisServer.delete(cacheKey);
    }

    public void setAccountGamesCache(Long accountId) {
        List<Game> games = gameRepository.findAllGameByAccountId(accountId, AccountStatusEnum.ACTIVE.name());
        if (games != null && !games.isEmpty()) {
            List<GameDTO> gameDTOList = dtoMapper.map(games, GameDTO.class);
            String cacheKey = String.format(GAME_ACCOUNT_CACHE_KEY, accountId);
            try {
                redisServer.add(cacheKey, objectMapper.writeValueAsString(gameDTOList), GAME_ACCOUNT_CACHE_TIME);
            } catch (Exception e) {
                log.error(String.format("Failed to add task type configuration rule %s to cache", cacheKey), e);
            }
        }
    }

    public void setGameCache(String gameKey) {
        Game game = gameRepository.findOneByKeyAndStatus(gameKey, AccountStatusEnum.ACTIVE);
        if (game != null) {
            GameDTO gameDTO = dtoMapper.map(game, GameDTO.class);
            String cacheKey = String.format(GAME_KEY_CACHE_KEY, gameKey);
            try {
                redisServer.add(cacheKey, objectMapper.writeValueAsString(gameDTO), GAME_KEY_CACHE_TIME);
            } catch (Exception e) {
                log.error(String.format("Failed to add task type configuration rule %s to cache", cacheKey), e);
            }
        }
    }

    public void setGameCache(Long gameId) {
        Game game = gameRepository.findOneByIdAndStatus(gameId, AccountStatusEnum.ACTIVE);
        if (game != null) {
            GameDTO gameDTO = dtoMapper.map(game, GameDTO.class);
            String cacheKey = String.format(GAME_ID_CACHE_KEY, gameId);
            try {
                redisServer.add(cacheKey, objectMapper.writeValueAsString(gameDTO), GAME_ID_CACHE_TIME);
            } catch (Exception e) {
                log.error(String.format("Failed to add task type configuration rule %s to cache", cacheKey), e);
            }
        }
    }

}
