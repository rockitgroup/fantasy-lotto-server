package com.rockitgroup.fantasylotto.domain.model.lotto.ticket;

import com.rockitgroup.infrastructure.vitamin.common.model.Deletable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
public class LottoTicketBall extends Deletable {

    @ManyToOne
    @JoinColumn
    private LottoTicket lottoTicket;

    private Integer position;
    private Integer value;
    private boolean specialBall;

}
