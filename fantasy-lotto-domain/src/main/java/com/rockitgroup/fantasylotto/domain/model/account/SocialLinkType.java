package com.rockitgroup.fantasylotto.domain.model.account;

import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
public class SocialLinkType extends Modifiable {

    private String typeKey;
    private String url;
}
