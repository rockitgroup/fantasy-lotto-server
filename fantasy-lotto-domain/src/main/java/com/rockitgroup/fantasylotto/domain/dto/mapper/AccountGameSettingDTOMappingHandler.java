package com.rockitgroup.fantasylotto.domain.dto.mapper;

import com.rockitgroup.fantasylotto.domain.dto.game.AccountGameSettingDTO;
import com.rockitgroup.fantasylotto.domain.model.game.AccountGameSetting;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.DTOMapper;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.MapperFacadeFactory;
import org.springframework.stereotype.Component;

@Component
public class AccountGameSettingDTOMappingHandler implements DTOMapper.MappingHandler<AccountGameSetting, AccountGameSettingDTO> {

    @Override
    public AccountGameSettingDTO map(AccountGameSetting model, Class<AccountGameSettingDTO> clazz) {
        return MapperFacadeFactory.create().map(model, clazz);
    }
}
