package com.rockitgroup.fantasylotto.domain.model.account;

import com.rockitgroup.fantasylotto.domain.model.enumeration.account.GroupPermissionEnum;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Group extends Modifiable {

    private String name;
    private boolean active;
    private String key;

    @ManyToMany(mappedBy = "groups", fetch = FetchType.LAZY)
    private Set<Account> accounts;

    @Enumerated(EnumType.STRING)
    @CollectionTable
    @Column
    @ElementCollection(targetClass = GroupPermissionEnum.class)
    private Set<GroupPermissionEnum> permissions;
}
