package com.rockitgroup.fantasylotto.domain.service.cache;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rockitgroup.fantasylotto.domain.dto.account.AccountTokenDTO;
import com.rockitgroup.fantasylotto.domain.model.account.AccountToken;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountStatusEnum;
import com.rockitgroup.fantasylotto.domain.repository.account.AccountTokenRepository;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.DTOMapper;
import com.rockitgroup.infrastructure.vitamin.common.redis.RedisServer;
import com.rockitgroup.infrastructure.vitamin.common.util.MapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AccountTokenCacheService {

    public final static String ACCOUNT_TOKEN_CACHE_KEY = "ACCOUNT_ID_%s_TOKEN_%s";

    public final static int ACCOUNT_TOKEN_CACHE_TIME = 60 * 5;

    @Autowired
    private RedisServer redisServer;

    @Autowired
    private AccountTokenRepository accountTokenRepository;

    @Autowired
    private DTOMapper dtoMapper;


    private ObjectMapper objectMapper = MapperUtils.getObjectMapper();


    public AccountTokenDTO getActiveToken(Long accountId, String token) {
        String cacheKey = String.format(ACCOUNT_TOKEN_CACHE_KEY, accountId, token);
        String cacheValue = redisServer.get(cacheKey);
        if (cacheValue == null) {
            setActiveTokenCache(accountId, token);
            cacheValue = redisServer.get(cacheKey);
        }

        if (cacheValue != null) {
            try {
                return objectMapper.readValue(cacheValue, AccountTokenDTO.class);
            } catch (Exception e) {
                log.error(String.format("Failed to read value for cache key %s", cacheKey), e);
            }
        }

        return null;
    }

    public void clearActiveTokenCache(Long accountId, String token) {
        String cacheKey = String.format(ACCOUNT_TOKEN_CACHE_KEY, accountId, token);
        redisServer.delete(cacheKey);
    }

    public void setActiveTokenCache(Long accountId, String token) {
        AccountToken accountToken =  accountTokenRepository.findOneByAccountIdAndTokenAndStatus(accountId, token, AccountStatusEnum.ACTIVE);
        if (accountToken != null) {
            AccountTokenDTO accountTokenDTO = dtoMapper.map(accountToken, AccountTokenDTO.class);
            String cacheKey = String.format(ACCOUNT_TOKEN_CACHE_KEY, accountId, token);
            try {
                redisServer.add(cacheKey, objectMapper.writeValueAsString(accountTokenDTO), ACCOUNT_TOKEN_CACHE_TIME);
            } catch (Exception e) {
                log.error(String.format("Failed to add task type configuration rule %s to cache", cacheKey), e);
            }
        }
    }

}
