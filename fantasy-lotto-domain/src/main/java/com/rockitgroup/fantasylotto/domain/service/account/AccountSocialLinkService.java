package com.rockitgroup.fantasylotto.domain.service.account;

import com.rockitgroup.fantasylotto.domain.repository.account.AccountSocialLinkRepository;
import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class AccountSocialLinkService extends DefaultBaseService {

    @Autowired
    private AccountSocialLinkRepository accountSocialLinkRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = accountSocialLinkRepository;
    }

}
