package com.rockitgroup.fantasylotto.domain.repository.lotto;

import com.rockitgroup.fantasylotto.domain.model.lotto.LottoConfig;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LottoConfigRepository extends BaseRepository<LottoConfig, Long> {


}

