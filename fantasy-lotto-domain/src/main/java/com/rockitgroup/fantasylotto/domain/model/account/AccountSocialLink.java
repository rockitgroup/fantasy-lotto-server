package com.rockitgroup.fantasylotto.domain.model.account;

import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountStatusEnum;
import com.rockitgroup.fantasylotto.domain.model.game.Game;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class AccountSocialLink extends Modifiable {

    @OneToOne
    private Account account;

    @OneToOne
    private SocialLinkType socialLinkType;

    @OneToOne
    private Game game;

    @Enumerated(EnumType.STRING)
    private AccountStatusEnum status;

    private String socialAccountId;
    private String socialAccountToken;

    private DateTime linkedAt;

}
