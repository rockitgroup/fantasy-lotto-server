package com.rockitgroup.fantasylotto.domain.dto.mapper;

import com.rockitgroup.fantasylotto.domain.dto.account.AccountDTO;
import com.rockitgroup.fantasylotto.domain.dto.game.AccountGameSettingDTO;
import com.rockitgroup.fantasylotto.domain.model.account.Account;
import com.rockitgroup.fantasylotto.domain.model.game.AccountGameSetting;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.DTOMapper;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.MapperFacadeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AccountDTOMappingHandler implements DTOMapper.MappingHandler<Account, AccountDTO> {

    @Autowired
    private DTOMapper dtoMapper;

    @Override
    public AccountDTO map(Account model, Class<AccountDTO> clazz) {
        AccountDTO result = MapperFacadeFactory.create().map(model, clazz);

        List<AccountGameSettingDTO> accountGameSettingDTOList = new ArrayList<>();

        if (model.getGameSettings() != null && !model.getGameSettings().isEmpty()) {
            for (AccountGameSetting accountGameSetting : model.getGameSettings()) {
                AccountGameSettingDTO accountGameSettingDTO = dtoMapper.map(accountGameSetting, AccountGameSettingDTO.class);
                accountGameSettingDTOList.add(accountGameSettingDTO);
            }
        }

        result.setSettings(accountGameSettingDTOList);

        return result;
    }
}
