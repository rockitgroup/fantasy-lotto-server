package com.rockitgroup.fantasylotto.domain.model.enumeration;

public enum  TrackingEventStatusEnum {
    PENDING, IN_PROGRESS, DONE
}
