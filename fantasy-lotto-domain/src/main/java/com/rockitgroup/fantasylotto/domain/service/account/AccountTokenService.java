package com.rockitgroup.fantasylotto.domain.service.account;

import com.rockitgroup.fantasylotto.domain.model.account.AccountToken;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountStatusEnum;
import com.rockitgroup.fantasylotto.domain.repository.account.AccountTokenRepository;
import com.rockitgroup.fantasylotto.domain.service.cache.AccountTokenCacheService;
import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class AccountTokenService extends DefaultBaseService {

    @Autowired
    private AccountTokenRepository accountTokenRepository;

    @Autowired
    private AccountTokenCacheService accountTokenCacheService;

    @PostConstruct
    public void postConstruct() {
        this.repository = accountTokenRepository;
    }

    public AccountToken findActiveToken(Long accountId, String token) {
        return accountTokenRepository.findOneByAccountIdAndTokenAndStatus(accountId, token, AccountStatusEnum.ACTIVE);
    }

    public void invalidateAllAccountTokens(Long accountId) {
        List<AccountToken> accountTokens = accountTokenRepository.findAllByAccountId(accountId);
        for (AccountToken accountToken : accountTokens) {
            accountToken.setStatus(AccountStatusEnum.INACTIVE);
        }
        save(accountTokens);
    }


    @Override
    protected void clearCache(Object o) {
        AccountToken accountToken = (AccountToken) o;
        accountTokenCacheService.clearActiveTokenCache(accountToken.getId(), accountToken.getToken());
    }

    @Override
    protected void clearCaches(List list) {
        for (Object o : list) {
            AccountToken accountToken = (AccountToken) o;
            accountTokenCacheService.clearActiveTokenCache(accountToken.getId(), accountToken.getToken());
        }
    }

}
