package com.rockitgroup.fantasylotto.domain.repository.lotto.game;

import com.rockitgroup.fantasylotto.domain.model.lotto.game.LottoGamePrize;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LottoGamePrizeRepository extends BaseRepository<LottoGamePrize, Long> {


}

