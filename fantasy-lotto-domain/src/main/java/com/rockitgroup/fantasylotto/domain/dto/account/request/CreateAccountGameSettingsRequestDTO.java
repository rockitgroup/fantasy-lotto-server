package com.rockitgroup.fantasylotto.domain.dto.account.request;

import com.rockitgroup.fantasylotto.domain.dto.game.AccountGameSettingDTO;
import com.rockitgroup.infrastructure.vitamin.common.dto.RequestDTO;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.Assert;

import java.util.List;

@Getter
@Setter
public class CreateAccountGameSettingsRequestDTO extends RequestDTO {

    private List<AccountGameSettingDTO> gameSettings;

    @Override
    public void validate() throws IllegalArgumentException {
        Assert.notNull(gameSettings, "Missing parameter gameSettings");

        if (gameSettings.isEmpty()) {
            throw new IllegalArgumentException("Parameter gameSettings cannot be empty");
        }
    }

}
