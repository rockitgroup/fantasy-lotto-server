package com.rockitgroup.fantasylotto.domain.model.lotto.game;


import com.rockitgroup.infrastructure.vitamin.common.model.Deletable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.DayOfWeek;

@Getter
@Setter
@Entity
public class LottoDrawSchedule extends Deletable {

    @ManyToOne
    @JoinColumn
    private LottoGame lottoGame;

    @Enumerated(EnumType.STRING)
    private DayOfWeek dayOfWeek;

    private Integer hour;
    private Integer minute;
}
