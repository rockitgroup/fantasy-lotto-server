package com.rockitgroup.fantasylotto.domain.model.enumeration.account;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 11/4/18
 * Time: 3:08 PM
 */
public enum AccountStatusEnum {

    ACTIVE,
    INACTIVE,
    PENDING,
    ERROR,
    CONFIRMED,
    EXPIRED
}
