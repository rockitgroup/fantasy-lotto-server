package com.rockitgroup.fantasylotto.domain.model.lotto.bet;

import com.rockitgroup.infrastructure.vitamin.common.model.Deletable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
public class LottoBetBall extends Deletable {

    @ManyToOne
    @JoinColumn
    private LottoBet lottoBet;

    private boolean cover;
    private Integer position;
    private Integer value;

}
