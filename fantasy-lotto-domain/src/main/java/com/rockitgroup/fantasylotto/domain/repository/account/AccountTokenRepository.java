package com.rockitgroup.fantasylotto.domain.repository.account;

import com.rockitgroup.fantasylotto.domain.model.account.AccountToken;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountStatusEnum;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountTokenRepository extends BaseRepository<AccountToken, Long> {

    List<AccountToken> findAllByAccountId(Long accountId);

    AccountToken findOneByAccountIdAndStatusOrderByCreatedAtDesc(Long accountId, AccountStatusEnum status);

    AccountToken findOneByAccountIdAndTokenAndStatus(Long accountId, String token, AccountStatusEnum status);
}
