package com.rockitgroup.fantasylotto.domain.repository.lotto.bet;

import com.rockitgroup.fantasylotto.domain.model.lotto.bet.LottoBetMode;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LottoBetModeRepository extends BaseRepository<LottoBetMode, Long> {


}

