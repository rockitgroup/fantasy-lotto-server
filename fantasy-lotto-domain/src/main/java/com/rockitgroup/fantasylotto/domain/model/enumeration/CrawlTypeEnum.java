package com.rockitgroup.fantasylotto.domain.model.enumeration;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 9/2/18
 * Time: 12:31 AM
 */
public enum CrawlTypeEnum {

    APP,
    ARTICLE_HOME_PAGE,
    ARTICLE_LISTING_PAGE,
    ARTICLE_DETAIL_PAGE
}
