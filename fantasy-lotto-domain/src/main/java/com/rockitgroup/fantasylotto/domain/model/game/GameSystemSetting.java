package com.rockitgroup.fantasylotto.domain.model.game;

import com.rockitgroup.fantasylotto.domain.model.enumeration.account.GameSystemTypeEnum;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class GameSystemSetting  extends Modifiable {

    @OneToOne
    private Game game;

    @Enumerated(EnumType.STRING)
    private GameSystemTypeEnum systemType;

    private String settingKey;
    private String settingValue;
}
