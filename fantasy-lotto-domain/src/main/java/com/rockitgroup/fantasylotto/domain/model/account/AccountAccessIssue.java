package com.rockitgroup.fantasylotto.domain.model.account;

import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccessIssueConfirmTypeEnum;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccessIssueTypeEnum;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountStatusEnum;
import com.rockitgroup.fantasylotto.domain.model.game.Game;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class AccountAccessIssue extends Modifiable {

    @OneToOne
    private Account account;

    @OneToOne
    private Game game;

    @Enumerated(EnumType.STRING)
    private AccountStatusEnum status;

    @Enumerated(EnumType.STRING)
    private AccessIssueTypeEnum accessIssueType;

    @Enumerated(EnumType.STRING)
    private AccessIssueConfirmTypeEnum confirmType;

    private String confirmCode;
    private String confirmContact;
    private String confirmIpAddress;
    private DateTime confirmedAt;
    private DateTime sentAt;

    private DateTime expiredAt;
    private String additionalInfo;

    public static AccountAccessIssue create(Account account, Game game, AccessIssueTypeEnum accessIssueType, AccessIssueConfirmTypeEnum confirmType, String confirmCode, String confirmContact, DateTime expiredAt) {
        AccountAccessIssue accountAccessIssue = new AccountAccessIssue();
        accountAccessIssue.setAccount(account);
        accountAccessIssue.setGame(game);
        accountAccessIssue.setAccessIssueType(accessIssueType);
        accountAccessIssue.setConfirmCode(confirmCode);
        accountAccessIssue.setConfirmContact(confirmContact);
        accountAccessIssue.setConfirmType(confirmType);
        accountAccessIssue.setStatus(AccountStatusEnum.PENDING);
        accountAccessIssue.setExpiredAt(expiredAt);
        return accountAccessIssue;
    }

}
