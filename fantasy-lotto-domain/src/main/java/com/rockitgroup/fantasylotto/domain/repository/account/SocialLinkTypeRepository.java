package com.rockitgroup.fantasylotto.domain.repository.account;

import com.rockitgroup.fantasylotto.domain.model.account.SocialLinkType;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SocialLinkTypeRepository extends BaseRepository<SocialLinkType, Long> {

    SocialLinkType findOneByTypeKey(String typeKey);
}
