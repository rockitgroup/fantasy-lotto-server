package com.rockitgroup.fantasylotto.domain.model.lotto.game;

import com.rockitgroup.infrastructure.vitamin.common.model.Deletable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class LottoGameResult extends Deletable {

    @OneToOne
    private LottoGame lottoGame;

    @OneToOne
    private LottoDrawEvent drawEvent;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gameResult", fetch = FetchType.LAZY)
    private List<LottoBallResult> resultBalls;

}
