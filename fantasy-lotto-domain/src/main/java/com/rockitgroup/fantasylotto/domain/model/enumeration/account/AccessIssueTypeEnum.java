package com.rockitgroup.fantasylotto.domain.model.enumeration.account;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 11/4/18
 * Time: 3:07 PM
 */
public enum AccessIssueTypeEnum {
    UPDATE_ACCOUNT_EMAIL,
    UPDATE_ACCOUNT_PHONE,
    LOGIN_EMAIL
}
