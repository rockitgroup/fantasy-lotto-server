package com.rockitgroup.fantasylotto.domain.model.game;

import com.rockitgroup.fantasylotto.domain.model.account.Account;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.GameTypeEnum;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountStatusEnum;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;


@Entity
@Getter
@Setter
public class Game extends Modifiable {

    private String name;
    private String key;
    private String description;

    @Enumerated(EnumType.STRING)
    private GameTypeEnum gameType;

    @Enumerated(EnumType.STRING)
    private AccountStatusEnum status;

    @ManyToMany(mappedBy = "games", fetch = FetchType.LAZY)
    private Set<Account> accounts;

    public void setGameId(Long id) {
        this.setId(id);
    }
}
