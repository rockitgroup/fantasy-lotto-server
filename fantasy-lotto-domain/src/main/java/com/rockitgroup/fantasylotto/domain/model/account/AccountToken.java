package com.rockitgroup.fantasylotto.domain.model.account;

import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountStatusEnum;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import java.util.UUID;

@Entity
@Getter
@Setter
public class AccountToken extends Modifiable {

    @OneToOne
    private Account account;

    @Enumerated(EnumType.STRING)
    private AccountStatusEnum status;

    private String token;

    public static AccountToken create(Account account) {
        AccountToken accountToken = new AccountToken();
        accountToken.setAccount(account);
        accountToken.setStatus(AccountStatusEnum.ACTIVE);
        accountToken.setToken(UUID.randomUUID().toString());
        return accountToken;
    }

    public void setAccountTokenId(Long id) {
        this.setId(id);
    }
}
