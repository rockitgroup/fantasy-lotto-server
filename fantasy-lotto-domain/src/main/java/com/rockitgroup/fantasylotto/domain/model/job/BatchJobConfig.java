package com.rockitgroup.fantasylotto.domain.model.job;

import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity
public class BatchJobConfig extends Modifiable {

    private String key;
    private String className;
    private String jobName;
    private String description;
    private boolean active;
    private String schedule;
    private String group;
    private String trigger;
}
