package com.rockitgroup.fantasylotto.domain.repository.site;

import com.rockitgroup.fantasylotto.domain.model.enumeration.SiteTypeEnum;
import com.rockitgroup.fantasylotto.domain.model.site.OriginSite;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OriginSiteRepository extends BaseRepository<OriginSite, Long> {

    OriginSite findBySiteKey(String siteKey);

    List<OriginSite> findBySiteTypeAndActiveIsTrueAndCrawlingIsTrue(SiteTypeEnum siteType);
}
