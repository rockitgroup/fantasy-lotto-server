package com.rockitgroup.fantasylotto.domain.repository.account;

import com.rockitgroup.fantasylotto.domain.model.account.Account;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountStatusEnum;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends BaseRepository<Account, Long> {

    Account findOneByStatusAndEmail(AccountStatusEnum status, String email);

}
