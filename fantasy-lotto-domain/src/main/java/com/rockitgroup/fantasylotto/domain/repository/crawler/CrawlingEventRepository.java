package com.rockitgroup.fantasylotto.domain.repository.crawler;

import com.rockitgroup.fantasylotto.domain.model.crawler.CrawlingEvent;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CrawlingEventRepository extends BaseRepository<CrawlingEvent, Long> {

}
