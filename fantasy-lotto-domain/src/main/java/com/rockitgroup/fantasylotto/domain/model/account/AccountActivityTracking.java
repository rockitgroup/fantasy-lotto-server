package com.rockitgroup.fantasylotto.domain.model.account;

import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountActivityTypeEnum;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountActivityResultEnum;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class AccountActivityTracking extends Modifiable {

    @OneToOne
    private Account account;

    @Enumerated(EnumType.STRING)
    private AccountActivityTypeEnum activityType;

    @Enumerated(EnumType.STRING)
    private AccountActivityResultEnum result;

    private String clientIpAddress;
    private String activityUrl;
    private String additionalInfo;

    public static AccountActivityTracking create(Account account, AccountActivityTypeEnum activityType, String clientIpAddress, AccountActivityResultEnum result, String activityUrl, String additionalInfo) {
        AccountActivityTracking accountActivityTracking = new AccountActivityTracking();
        accountActivityTracking.setAccount(account);
        accountActivityTracking.setActivityType(activityType);
        accountActivityTracking.setClientIpAddress(clientIpAddress);
        accountActivityTracking.setResult(result);
        accountActivityTracking.setActivityUrl(activityUrl);
        accountActivityTracking.setAdditionalInfo(additionalInfo);
        return accountActivityTracking;
    }

}
