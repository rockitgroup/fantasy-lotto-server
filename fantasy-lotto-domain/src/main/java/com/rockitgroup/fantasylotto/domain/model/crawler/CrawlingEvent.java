package com.rockitgroup.fantasylotto.domain.model.crawler;

import com.rockitgroup.fantasylotto.domain.model.enumeration.CrawlTypeEnum;
import com.rockitgroup.fantasylotto.domain.model.enumeration.TrackingEventResultEnum;
import com.rockitgroup.fantasylotto.domain.model.enumeration.TrackingEventStatusEnum;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@Getter
@Setter
public class CrawlingEvent extends Modifiable {

    private DateTime crawlStartedAt;
    private DateTime crawlEndedAt;
    private String crawlUrl;

    @Enumerated(EnumType.STRING)
    private TrackingEventResultEnum result;

    @Enumerated(EnumType.STRING)
    private TrackingEventStatusEnum status;

    @Enumerated(EnumType.STRING)
    private CrawlTypeEnum crawlType;

    public static CrawlingEvent create(String crawlUrl, CrawlTypeEnum crawlType) {
        CrawlingEvent crawlingEvent = new CrawlingEvent();
        crawlingEvent.setCrawlStartedAt(DateTime.now());
        crawlingEvent.setCrawlUrl(crawlUrl);
        crawlingEvent.setCrawlType(crawlType);
        crawlingEvent.setStatus(TrackingEventStatusEnum.IN_PROGRESS);
        return crawlingEvent;
    }

    public static CrawlingEvent complete(CrawlingEvent crawlingEvent, TrackingEventResultEnum result) {
        crawlingEvent.setCrawlEndedAt(DateTime.now());
        crawlingEvent.setResult(result);
        crawlingEvent.setStatus(TrackingEventStatusEnum.DONE);
        return crawlingEvent;
    }
}
