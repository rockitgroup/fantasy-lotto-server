package com.rockitgroup.fantasylotto.domain.model.lotto.game;

import com.rockitgroup.infrastructure.vitamin.common.model.Deletable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
public class LottoGamePrize extends Deletable {

    private String name;

    private Double prizeAmount;

    private Integer numNormalWinningBall;

    private boolean winningSpecialBall;

    private boolean grandPrize;

    @ManyToOne
    @JoinColumn
    private LottoGame lottoGame;
}
