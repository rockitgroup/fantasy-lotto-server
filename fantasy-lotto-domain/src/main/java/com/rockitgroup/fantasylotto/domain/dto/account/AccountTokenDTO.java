package com.rockitgroup.fantasylotto.domain.dto.account;

import com.rockitgroup.infrastructure.vitamin.common.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountTokenDTO extends BaseDTO {

    private Long accountId;
    private String status;
    private String token;
}
