package com.rockitgroup.fantasylotto.domain.service.game;

import com.rockitgroup.fantasylotto.domain.model.game.AccountGameSetting;
import com.rockitgroup.fantasylotto.domain.repository.game.AccountGameSettingRepository;
import com.rockitgroup.fantasylotto.domain.service.cache.AccountCacheService;
import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class AccountGameSettingService extends DefaultBaseService {

    @Autowired
    private AccountGameSettingRepository accountGameSettingRepository;

    @Autowired
    private AccountCacheService accountCacheService;

    @PostConstruct
    public void postConstruct() {
        this.repository = accountGameSettingRepository;
    }

    public List<AccountGameSetting> findBySettingKey(Long accountId, Long gameId, String settingKey) {
        return accountGameSettingRepository.findAllByAccountIdAndGameIdAndSettingKey(accountId, gameId, settingKey);
    }

    public void deleteDuplicateSettingKey(Long accountId, Long gameId, String settingKey) {
        List<AccountGameSetting> accountGameSettings = findBySettingKey(accountId, gameId, settingKey);
        accountGameSettingRepository.deleteInBatch(accountGameSettings);
        clearCaches(accountGameSettings);
    }

    @Override
    protected void clearCache(Object o) {
        AccountGameSetting accountGameSetting = (AccountGameSetting) o;
        accountCacheService.clearAccountCache(accountGameSetting.getAccount().getId());
    }

    @Override
    protected void clearCaches(List list) {
        for (Object o : list) {
            AccountGameSetting accountGameSetting = (AccountGameSetting) o;
            accountCacheService.clearAccountCache(accountGameSetting.getAccount().getId());
        }
    }
}
