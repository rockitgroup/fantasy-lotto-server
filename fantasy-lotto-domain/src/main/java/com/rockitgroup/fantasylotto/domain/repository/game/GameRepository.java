package com.rockitgroup.fantasylotto.domain.repository.game;

import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountStatusEnum;
import com.rockitgroup.fantasylotto.domain.model.game.Game;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameRepository extends BaseRepository<Game, Long> {

    Game findOneByIdAndStatus(Long id, AccountStatusEnum status);

    Game findOneByKeyAndStatus(String key, AccountStatusEnum status);

    @Query(value = "SELECT * FROM games g INNER JOIN account_games ag ON ag.gamesId=g.id WHERE g.status=:status AND ag.accountsId=:accountId", nativeQuery = true)
    List<Game> findAllGameByAccountId(@Param("accountId") Long accountId, @Param("status") String status);
}
