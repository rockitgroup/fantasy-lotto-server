package com.rockitgroup.fantasylotto.domain.repository.lotto.ticket;

import com.rockitgroup.fantasylotto.domain.model.lotto.ticket.LottoTicketBall;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LottoTicketBallRepository extends BaseRepository<LottoTicketBall, Long> {


}

