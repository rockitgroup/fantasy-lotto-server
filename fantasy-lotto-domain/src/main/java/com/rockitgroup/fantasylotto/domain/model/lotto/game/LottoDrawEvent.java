package com.rockitgroup.fantasylotto.domain.model.lotto.game;

import com.rockitgroup.infrastructure.vitamin.common.model.Deletable;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Getter
@Setter
@Entity
public class LottoDrawEvent extends Deletable {

    @OneToOne
    private LottoGame lottoGame;

    private DateTime drawAt;
}
