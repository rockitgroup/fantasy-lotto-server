package com.rockitgroup.fantasylotto.domain.model.enumeration.account;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 11/11/18
 * Time: 12:07 AM
 */
public enum GameSystemTypeEnum {
    UPDATE_ACCOUNT_EMAIL,
    LOGIN_EMAIL
}
