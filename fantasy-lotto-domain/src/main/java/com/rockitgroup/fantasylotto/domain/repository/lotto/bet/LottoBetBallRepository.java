package com.rockitgroup.fantasylotto.domain.repository.lotto.bet;

import com.rockitgroup.fantasylotto.domain.model.lotto.bet.LottoBetBall;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LottoBetBallRepository extends BaseRepository<LottoBetBall, Long> {


}
