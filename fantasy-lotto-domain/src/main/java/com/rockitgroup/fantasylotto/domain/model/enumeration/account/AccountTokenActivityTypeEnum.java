package com.rockitgroup.fantasylotto.domain.model.enumeration.account;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 11/4/18
 * Time: 3:45 PM
 */
public enum AccountTokenActivityTypeEnum {

    VALIDATE,
    UPDATE_ACCOUNT_EMAIL,
    CONFIRM_UPDATE_ACCOUNT_EMAIL,
    GET_ACCOUNT_INFO,
    CREATE_ACCOUNT_GAME_SETTINGS,
    LINK_SOCIAL_ACCOUNT
}
