package com.rockitgroup.fantasylotto.domain.repository.job;

import com.rockitgroup.fantasylotto.domain.model.job.BatchJobTracking;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BatchJobTrackingRepository extends BaseRepository<BatchJobTracking, Long> {

}
