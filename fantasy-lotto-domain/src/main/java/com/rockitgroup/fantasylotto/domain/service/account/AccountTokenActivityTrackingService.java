package com.rockitgroup.fantasylotto.domain.service.account;

import com.rockitgroup.fantasylotto.domain.repository.account.AccountTokenActivityTrackingRepository;
import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class AccountTokenActivityTrackingService extends DefaultBaseService {

    @Autowired
    private AccountTokenActivityTrackingRepository accountTokenActivityTrackingRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = accountTokenActivityTrackingRepository;
    }



}
