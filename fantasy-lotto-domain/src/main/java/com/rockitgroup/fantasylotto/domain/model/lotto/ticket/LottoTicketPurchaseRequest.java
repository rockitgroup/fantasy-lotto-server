package com.rockitgroup.fantasylotto.domain.model.lotto.ticket;

import com.rockitgroup.fantasylotto.domain.model.enumeration.LottoTicketPurchaseRequestStatusEnum;
import com.rockitgroup.infrastructure.vitamin.common.model.Deletable;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class LottoTicketPurchaseRequest extends Deletable {

    private Long requestById;

    private Double purchaseAmount;
    private Double feeAmount;
    private Double totalFinalAmount;

    private DateTime requestAt;

    @Enumerated(EnumType.STRING)
    private LottoTicketPurchaseRequestStatusEnum status;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "purchaseRequest", fetch = FetchType.LAZY)
    private List<LottoTicket> tickets;
}
