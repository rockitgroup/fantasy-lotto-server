package com.rockitgroup.fantasylotto.domain.repository.lotto.bet;

import com.rockitgroup.fantasylotto.domain.model.lotto.bet.LottoBet;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LottoBetRepository extends BaseRepository<LottoBet, Long> {


}
