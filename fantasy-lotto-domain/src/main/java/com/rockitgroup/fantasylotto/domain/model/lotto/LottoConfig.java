package com.rockitgroup.fantasylotto.domain.model.lotto;

import com.rockitgroup.infrastructure.vitamin.common.model.Deletable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity
public class LottoConfig extends Deletable {

    private String configKey;
    private String configValue;
}
