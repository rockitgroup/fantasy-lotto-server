package com.rockitgroup.fantasylotto.domain.service.account;

import com.rockitgroup.fantasylotto.domain.model.account.Account;
import com.rockitgroup.fantasylotto.domain.model.account.AccountAccessIssue;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.*;
import com.rockitgroup.fantasylotto.domain.model.game.Game;
import com.rockitgroup.fantasylotto.domain.model.game.GameSystemSetting;
import com.rockitgroup.fantasylotto.domain.repository.account.AccountAccessIssueRepository;
import com.rockitgroup.fantasylotto.domain.service.game.GameSystemSettingService;
import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import com.rockitgroup.infrastructure.vitamin.common.service.EmailService;
import com.rockitgroup.infrastructure.vitamin.common.util.NumberUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.core.lookup.StrSubstitutor;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.mail.internet.InternetAddress;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class AccountAccessIssueService extends DefaultBaseService {

    private static final int CONFIRM_CODE_EXPIRATION_TOTAL_MINUTES = 30;

    @Resource(name = "fromEmailAddress")
    private String fromEmailAddress;

    @Autowired
    private EmailService emailService;

    @Autowired
    private GameSystemSettingService gameSystemSettingService;

    @Autowired
    private AccountAccessIssueRepository accountAccessIssueRepository;

    @Autowired
    private AsyncTaskExecutor asyncTaskExecutor;

    @PostConstruct
    public void postConstruct() {
        this.repository = accountAccessIssueRepository;
    }

    public AccountAccessIssue findActiveIssueByConfirmCode(Long accountId, Long gameId, AccessIssueTypeEnum issueType, AccessIssueConfirmTypeEnum confirmType, String confirmCode) {
        AccountAccessIssue accountAccessIssue = accountAccessIssueRepository.findIssueByConfirmCode(accountId, gameId, AccountStatusEnum.ACTIVE.name(), issueType.name(), confirmType.name(), confirmCode);
        if (accountAccessIssue != null && accountAccessIssue.getExpiredAt().isBeforeNow()) {
            accountAccessIssue.setStatus(AccountStatusEnum.EXPIRED);
            accountAccessIssueRepository.saveAndFlush(accountAccessIssue);
            return null;
        }

        return accountAccessIssue;
    }

    public void confirmIssue(AccountAccessIssue accountAccessIssue, String confirmIpAddress) {
        accountAccessIssue.setStatus(AccountStatusEnum.CONFIRMED);
        accountAccessIssue.setConfirmedAt(DateTime.now());
        accountAccessIssue.setConfirmIpAddress(confirmIpAddress);
        accountAccessIssueRepository.saveAndFlush(accountAccessIssue);
    }

    private void clearExistingIssueOfSameType(Account account, Game game, AccessIssueTypeEnum accessIssueType) {
        List<AccountAccessIssue> accountAccessIssues = accountAccessIssueRepository.findAllByAccountIdAndGameIdAndAccessIssueTypeAndStatus(account.getId(), game.getId(), accessIssueType, AccountStatusEnum.ACTIVE);
        for (AccountAccessIssue accountAccessIssue : accountAccessIssues) {
            accountAccessIssue.setStatus(AccountStatusEnum.INACTIVE);
        }
        accountAccessIssueRepository.saveAll(accountAccessIssues);
    }

    public void createAccountAccessIssueEmailType(Account account, Game game, String email, AccessIssueTypeEnum accessIssueTypeEnum, GameSystemTypeEnum gameSystemTypeEnum, String emailTemplate) throws UnsupportedEncodingException {
        clearExistingIssueOfSameType(account, game, accessIssueTypeEnum);

        String confirmCode = NumberUtils.getRandomNumberCode();
        AccountAccessIssue accountAccessIssue = AccountAccessIssue.create(account, game, accessIssueTypeEnum, AccessIssueConfirmTypeEnum.EMAIL, confirmCode, email, DateTime.now().plusMinutes(CONFIRM_CODE_EXPIRATION_TOTAL_MINUTES));
        accountAccessIssue = accountAccessIssueRepository.save(accountAccessIssue);

        GameSystemSetting gameSystemSettingEmailSubject = gameSystemSettingService.findSettingValue(game.getId(), gameSystemTypeEnum, GameSystemSettingKeyEnum.EMAIL_SUBJECT.getValue());

        if (gameSystemSettingEmailSubject == null) {
            accountAccessIssue.setStatus(AccountStatusEnum.ERROR);
            accountAccessIssue.setAdditionalInfo("Missing subject data in game system settings table");
            accountAccessIssueRepository.save(accountAccessIssue);

            log.error("Missing subject data in game system settings table");

            throw new DataRetrievalFailureException("System error. Database recheck needed");
        }

        sendEmailConfirmation(accountAccessIssue, gameSystemSettingEmailSubject, emailTemplate);
    }

    private void sendEmailConfirmation(AccountAccessIssue accountAccessIssue, GameSystemSetting gameSystemSettingEmailSubject, String template) throws UnsupportedEncodingException {
        Map<String, String> data = new HashMap<>();
        data.put("confirmCode", accountAccessIssue.getConfirmCode());
        data.put("game", accountAccessIssue.getGame().getName());
        data.put("expiredMinutes", Integer.toString(CONFIRM_CODE_EXPIRATION_TOTAL_MINUTES));

        StrSubstitutor sub = new StrSubstitutor(data, "%(", ")");
        String subject = sub.replace(gameSystemSettingEmailSubject.getSettingValue());
        InternetAddress fromEmail = new InternetAddress(fromEmailAddress, accountAccessIssue.getGame().getName());

        asyncTaskExecutor.submit(() -> {
            try {
                emailService.sendEmails(fromEmail, Collections.singletonList(accountAccessIssue.getConfirmContact()), subject, template, data, false);

                accountAccessIssue.setSentAt(DateTime.now());
                accountAccessIssue.setStatus(AccountStatusEnum.ACTIVE);
            } catch (Exception e) {
                log.error("Failed to send email confirmation", e);
                accountAccessIssue.setStatus(AccountStatusEnum.ERROR);
                accountAccessIssue.setAdditionalInfo(e.getMessage());
            }

            accountAccessIssueRepository.save(accountAccessIssue);
        });
    }

}
