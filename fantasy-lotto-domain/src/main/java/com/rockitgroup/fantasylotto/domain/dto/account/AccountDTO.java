package com.rockitgroup.fantasylotto.domain.dto.account;

import com.rockitgroup.fantasylotto.domain.dto.game.AccountGameSettingDTO;
import com.rockitgroup.infrastructure.vitamin.common.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AccountDTO extends BaseDTO {

    private String email;
    private String phone;
    private String status;
    private Integer money;
    private List<AccountGameSettingDTO> settings;
}
