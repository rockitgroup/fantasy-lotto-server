package com.rockitgroup.fantasylotto.domain.model.lotto.bet;

import com.rockitgroup.fantasylotto.domain.model.enumeration.LottoBetStatusEnum;
import com.rockitgroup.fantasylotto.domain.model.lotto.game.LottoDrawEvent;
import com.rockitgroup.fantasylotto.domain.model.lotto.game.LottoGame;
import com.rockitgroup.infrastructure.vitamin.common.model.Deletable;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class LottoBet extends Deletable {

    @OneToOne
    private LottoGame lottoGame;

    private Long betById;

    @OneToOne
    private LottoBetMode betMode;

    @OneToOne
    private LottoDrawEvent betDrawEvent;

    private Double betAmount;
    private Integer betNumber;
    private DateTime betAt;

    @Enumerated(EnumType.STRING)
    private LottoBetStatusEnum status;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lottoBet", fetch = FetchType.LAZY)
    private List<LottoBetBall> balls;


}
