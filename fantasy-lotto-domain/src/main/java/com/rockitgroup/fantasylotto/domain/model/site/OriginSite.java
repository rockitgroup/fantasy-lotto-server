package com.rockitgroup.fantasylotto.domain.model.site;

import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.rockitgroup.fantasylotto.domain.model.enumeration.SiteTypeEnum;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.net.URL;
import java.util.List;

@Getter
@Setter
@Entity
@Cacheable
public class OriginSite extends Modifiable {

	@NotNull
	@Column(unique = true)
	private String siteKey;

	@Column(unique = true)
	private String name;

	@NotNull
	private boolean crawling;

    @NotNull
	private boolean active;

    @Enumerated(EnumType.STRING)
    private SiteTypeEnum siteType;

    private String homeUrl;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "originSite", fetch = FetchType.LAZY)
	private List<OriginSiteUrl> originSiteUrls;

	protected OriginSite() {
	}

	public static OriginSite create(String name, String siteKey, URL url) {
		OriginSite result = new OriginSite();
		result.name = name;
		result.siteKey = siteKey.toUpperCase();
		result.active = true;
		result.crawling = false;
		result.originSiteUrls = Lists.newArrayList(OriginSiteUrl.create(result, url));
		return result;
	}

	public Optional<OriginSiteUrl> get(final URL url) {
		return Iterables.tryFind(originSiteUrls, input -> input.getUrl().toString().equals(url.toString()));
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public void setCrawling(boolean crawling) {
		this.crawling = crawling;
	}

	public void setOriginSiteUrls(List<OriginSiteUrl> originSiteUrls) {
		this.originSiteUrls = originSiteUrls;
	}
}
