package com.rockitgroup.fantasylotto.domain.dto.account.request;

import com.rockitgroup.infrastructure.vitamin.common.dto.RequestDTO;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.Assert;

@Getter
@Setter
public class LinkSocialRequestDTO extends RequestDTO {

    private String socialAccountId;
    private String socialAccountToken;
    private String socialLinkType;

    @Override
    public void validate() throws IllegalArgumentException {
        Assert.notNull(socialAccountId, "Missing parameter socialAccountId");
        Assert.notNull(socialAccountToken, "Missing parameter socialAccountToken");
        Assert.notNull(socialLinkType, "Missing parameter socialLinkType");
    }
}
