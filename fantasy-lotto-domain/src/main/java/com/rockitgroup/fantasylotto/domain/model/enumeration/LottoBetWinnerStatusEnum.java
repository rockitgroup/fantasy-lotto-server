package com.rockitgroup.fantasylotto.domain.model.enumeration;

public enum  LottoBetWinnerStatusEnum {

    PENDING, CONFIRMED, INVALID_FRAUD
}
