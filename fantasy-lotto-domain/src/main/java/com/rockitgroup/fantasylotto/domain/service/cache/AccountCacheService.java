package com.rockitgroup.fantasylotto.domain.service.cache;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rockitgroup.fantasylotto.domain.dto.account.AccountDTO;
import com.rockitgroup.fantasylotto.domain.model.account.Account;
import com.rockitgroup.fantasylotto.domain.repository.account.AccountRepository;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.DTOMapper;
import com.rockitgroup.infrastructure.vitamin.common.redis.RedisServer;
import com.rockitgroup.infrastructure.vitamin.common.util.MapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class AccountCacheService {

    public final static String ACCOUNT_CACHE_KEY = "ACCOUNT_ID_%s";

    public final static int ACCOUNT_CACHE_TIME = 60 * 5;

    @Autowired
    private RedisServer redisServer;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private DTOMapper dtoMapper;

    private ObjectMapper objectMapper = MapperUtils.getObjectMapper();

    public AccountDTO getAccount(Long accountId) {
        String cacheKey = String.format(ACCOUNT_CACHE_KEY, accountId);
        String cacheValue = redisServer.get(cacheKey);
        if (cacheValue == null) {
            setAccountCache(accountId);
            cacheValue = redisServer.get(cacheKey);
        }

        if (cacheValue != null) {
            try {
                return objectMapper.readValue(cacheValue, AccountDTO.class);
            } catch (Exception e) {
                log.error(String.format("Failed to read value for cache key %s", cacheKey), e);
            }
        }

        return null;
    }

    public void clearAccountCache(Long accountId) {
        String cacheKey = String.format(ACCOUNT_CACHE_KEY, accountId);
        redisServer.delete(cacheKey);
    }

    public void setAccountCache(Long accountId) {
        Optional<Account> account =  accountRepository.findById(accountId);
        if (account.isPresent()) {
            AccountDTO accountDTO = dtoMapper.map(account.get(), AccountDTO.class);
            String cacheKey = String.format(ACCOUNT_CACHE_KEY, accountId);
            try {
                redisServer.add(cacheKey, objectMapper.writeValueAsString(accountDTO), ACCOUNT_CACHE_TIME);
            } catch (Exception e) {
                log.error(String.format("Failed to add task type configuration rule %s to cache", cacheKey), e);
            }
        }
    }
}
