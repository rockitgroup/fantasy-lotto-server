package com.rockitgroup.fantasylotto.domain.model.enumeration;

public enum  LottoGameStatusEnum {

    ACTIVE, INACTIVE
}
