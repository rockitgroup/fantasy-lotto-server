package com.rockitgroup.fantasylotto.domain.repository.account;

import com.rockitgroup.fantasylotto.domain.model.account.Group;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends BaseRepository<Group, Long> {
}
