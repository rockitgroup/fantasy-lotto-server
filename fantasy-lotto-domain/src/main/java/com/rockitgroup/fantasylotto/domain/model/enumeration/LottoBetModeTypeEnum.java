package com.rockitgroup.fantasylotto.domain.model.enumeration;

public enum  LottoBetModeTypeEnum {

    PLUS_BALL, CONCATENATE_BALL
}
