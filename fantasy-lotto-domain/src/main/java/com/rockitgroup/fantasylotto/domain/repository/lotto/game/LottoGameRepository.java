package com.rockitgroup.fantasylotto.domain.repository.lotto.game;

import com.rockitgroup.fantasylotto.domain.model.lotto.game.LottoGame;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LottoGameRepository extends BaseRepository<LottoGame, Long> {


}

