package com.rockitgroup.fantasylotto.domain.configuration;

import com.rockitgroup.infrastructure.vitamin.common.configuration.BaseSecurityConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends BaseSecurityConfig {
}
