package com.rockitgroup.fantasylotto.domain.service.game;

import com.rockitgroup.fantasylotto.domain.dto.game.GameDTO;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.GameTypeEnum;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountStatusEnum;
import com.rockitgroup.fantasylotto.domain.model.game.Game;
import com.rockitgroup.fantasylotto.domain.repository.game.GameRepository;
import com.rockitgroup.fantasylotto.domain.service.cache.GameCacheService;
import com.rockitgroup.infrastructure.vitamin.common.service.DefaultBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class GameService extends DefaultBaseService {

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private GameCacheService gameCacheService;

    @PostConstruct
    public void postConstruct() {
        this.repository = gameRepository;
    }

    public Game findActiveGameById(Long gameId) {
        GameDTO gameDTO = gameCacheService.getGame(gameId);
        if (gameDTO != null) {
            return convertDtoToModel(gameDTO);
        }

        return gameRepository.findOneByIdAndStatus(gameId, AccountStatusEnum.ACTIVE);

    }

    public Game findActiveGameByKey(String key) {
        return gameRepository.findOneByKeyAndStatus(key, AccountStatusEnum.ACTIVE);
    }

    public List<Game> getAllGames() {
        return gameRepository.findAll();
    }

    public Game convertDtoToModel(GameDTO gameDTO) {
        Game game = new Game();
        game.setGameId(gameDTO.getId());
        game.setGameType(GameTypeEnum.valueOf(gameDTO.getGameType()));
        game.setKey(gameDTO.getKey());
        game.setName(gameDTO.getName());
        game.setStatus(AccountStatusEnum.valueOf(gameDTO.getStatus()));
        game.setDescription(gameDTO.getDescription());
        return game;
    }

    @Override
    protected void clearCache(Object o) {
        Game game = (Game) o;
        gameCacheService.clearGameCache(game.getId());
    }

    @Override
    protected void clearCaches(List list) {
        for (Object o : list) {
            Game game = (Game) o;
            gameCacheService.clearGameCache(game.getId());
        }
    }
}
