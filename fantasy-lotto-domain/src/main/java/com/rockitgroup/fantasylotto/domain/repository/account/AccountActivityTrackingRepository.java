package com.rockitgroup.fantasylotto.domain.repository.account;

import com.rockitgroup.fantasylotto.domain.model.account.AccountActivityTracking;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountActivityTrackingRepository extends BaseRepository<AccountActivityTracking, Long> {
}
