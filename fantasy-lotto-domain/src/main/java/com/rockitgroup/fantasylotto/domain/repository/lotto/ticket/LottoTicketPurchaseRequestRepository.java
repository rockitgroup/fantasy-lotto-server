package com.rockitgroup.fantasylotto.domain.repository.lotto.ticket;

import com.rockitgroup.fantasylotto.domain.model.lotto.ticket.LottoTicketPurchaseRequest;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface  LottoTicketPurchaseRequestRepository extends BaseRepository<LottoTicketPurchaseRequest, Long> {


}

