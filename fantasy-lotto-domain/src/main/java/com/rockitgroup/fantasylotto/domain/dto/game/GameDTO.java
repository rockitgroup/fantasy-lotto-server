package com.rockitgroup.fantasylotto.domain.dto.game;

import com.rockitgroup.infrastructure.vitamin.common.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GameDTO extends BaseDTO {

    private String name;
    private String key;
    private String description;
    private String gameType;
    private String status;

}
