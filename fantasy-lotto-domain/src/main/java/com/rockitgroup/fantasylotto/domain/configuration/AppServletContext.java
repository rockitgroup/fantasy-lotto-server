package com.rockitgroup.fantasylotto.domain.configuration;

import com.rockitgroup.infrastructure.vitamin.common.configuration.context.BaseApiServletContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
public class AppServletContext extends BaseApiServletContext {
}
