package com.rockitgroup.fantasylotto.domain.model.enumeration;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 10/14/18
 * Time: 4:53 PM
 */
public enum TrackingEventResultEnum {
    SUCCESS, FAIL
}
