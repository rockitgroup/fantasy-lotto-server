package com.rockitgroup.fantasylotto.domain.dto.mapper;

import com.rockitgroup.fantasylotto.domain.dto.account.AccountTokenDTO;
import com.rockitgroup.fantasylotto.domain.model.account.AccountToken;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.DTOMapper;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.MapperFacadeFactory;
import org.springframework.stereotype.Component;

@Component
public class AccountTokenDTOMappingHandler implements DTOMapper.MappingHandler<AccountToken, AccountTokenDTO> {

    @Override
    public AccountTokenDTO map(AccountToken model, Class<AccountTokenDTO> clazz) {
        AccountTokenDTO accountTokenDTO = MapperFacadeFactory.create().map(model, clazz);
        accountTokenDTO.setAccountId(model.getAccount().getId());
        accountTokenDTO.setToken(model.getToken());
        accountTokenDTO.setStatus(model.getStatus().name());
        return accountTokenDTO;
    }
}
