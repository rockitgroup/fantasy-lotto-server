package com.rockitgroup.fantasylotto.domain.dto.account.request;

import com.rockitgroup.infrastructure.vitamin.common.dto.RequestDTO;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.Assert;

@Getter
@Setter
public class RegisterAccountDeviceRequestDTO extends RequestDTO {

    private String deviceUniqueId;
    private String deviceSecretKey;
    private String deviceOSVersion;
    private String deviceModel;

    @Override
    public void validate() throws IllegalArgumentException {
        Assert.notNull(deviceUniqueId, "Missing parameter device unique id");
        Assert.notNull(deviceSecretKey, "Missing parameter device secret key");
    }
}
