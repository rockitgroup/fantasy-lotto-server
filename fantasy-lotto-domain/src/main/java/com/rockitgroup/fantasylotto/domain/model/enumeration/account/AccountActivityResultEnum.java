package com.rockitgroup.fantasylotto.domain.model.enumeration.account;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 11/5/18
 * Time: 2:30 AM
 */
public enum AccountActivityResultEnum {
    SUCCESS, FAIL
}
