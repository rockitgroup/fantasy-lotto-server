package com.rockitgroup.fantasylotto.domain.model.lotto.game;

import com.rockitgroup.fantasylotto.domain.model.enumeration.LottoGameKeyEnum;
import com.rockitgroup.fantasylotto.domain.model.enumeration.LottoGameStatusEnum;
import com.rockitgroup.infrastructure.vitamin.common.model.Deletable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class LottoGame extends Deletable {

    private String name;
    private String description;

    private String iconImageUrl;

    private Integer totalNormalBall;
    private Integer normalBallMin;
    private Integer normalBallMax;

    private Boolean haveSpecialBall;
    private Integer specialBallMin;
    private Integer specialBallMax;

    @Enumerated(EnumType.STRING)
    private LottoGameStatusEnum status;

    @Enumerated(EnumType.STRING)
    private LottoGameKeyEnum key;

    @OneToOne
    private LottoDrawEvent nextDrawEvent;

    @OneToOne
    private LottoGameResult latestResult;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lottoGame", fetch = FetchType.LAZY)
    private List<LottoDrawSchedule> schedules;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lottoGame", fetch = FetchType.LAZY)
    private List<LottoGamePrize> prizes;

}
