package com.rockitgroup.fantasylotto.interfaces.controller;

import com.rockitgroup.fantasylotto.domain.model.account.Account;
import com.rockitgroup.fantasylotto.domain.model.account.AccountActivityTracking;
import com.rockitgroup.fantasylotto.domain.model.account.AccountToken;
import com.rockitgroup.fantasylotto.domain.model.account.AccountTokenActivityTracking;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountActivityResultEnum;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountActivityTypeEnum;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountTokenActivityTypeEnum;
import com.rockitgroup.fantasylotto.domain.model.game.Game;
import com.rockitgroup.fantasylotto.domain.service.account.AccountActivityTrackingService;
import com.rockitgroup.fantasylotto.domain.service.account.AccountTokenActivityTrackingService;
import com.rockitgroup.fantasylotto.domain.service.account.AccountTokenService;
import com.rockitgroup.fantasylotto.domain.service.game.GameService;
import com.rockitgroup.infrastructure.vitamin.common.controller.BaseController;
import com.rockitgroup.infrastructure.vitamin.common.dto.ResponseDTO;
import com.rockitgroup.infrastructure.vitamin.common.exception.BadRequestException;
import com.rockitgroup.infrastructure.vitamin.common.exception.NotFoundException;
import com.rockitgroup.infrastructure.vitamin.common.exception.UnauthorizedException;
import com.rockitgroup.infrastructure.vitamin.common.redis.RedisServer;
import com.rockitgroup.infrastructure.vitamin.common.util.WebUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.Set;

@Slf4j
public abstract class AppBaseController extends BaseController {

    private static final String AUTHENTICATE_REQUEST_CACHE_KEY = "AUTHENTICATE_REQUEST_%s";
    private static final int AUTHENTICATE_REQUEST_CACHE_TIME = 60; // Timeout in 1 minute

    @Autowired
    private RedisServer redisServer;

    @Autowired
    private AccountTokenService accountTokenService;

    @Autowired
    private AccountActivityTrackingService accountActivityTrackingService;

    @Autowired
    private AccountTokenActivityTrackingService accountTokenActivityTrackingService;

    @Autowired
    private GameService gameService;


    protected ResponseDTO generateFailResponse(HttpServletRequest servletRequest, ResponseDTO responseDTO, AccountToken accountToken, AccountTokenActivityTypeEnum accountTokenActivityType, Throwable e) {
        logAccountTokenErrorActivity(servletRequest, accountTokenActivityType, accountToken, e.getMessage());
        return generateFailResponse(servletRequest, responseDTO, e);
    }

    protected ResponseDTO generateFailResponse(HttpServletRequest servletRequest, ResponseDTO responseDTO, Account account, AccountActivityTypeEnum accountActivityType, Throwable e) {
        logAccountErrorActivity(servletRequest, accountActivityType, account, e.getMessage());
        return generateFailResponse(servletRequest, responseDTO, e);
    }

    protected Game validateGame(Long gameId) {
        Game game = gameService.findActiveGameById(gameId);
        if (game == null) {
            throw new NotFoundException("Game does not exist or not active");
        }
        return game;
    }

    protected AccountToken authenticateTokenRequest(HttpServletRequest request, Long gameId) {
        AccountToken accountToken = getAccountTokenFromRequest(request);
        if (accountToken == null) {
            throw new UnauthorizedException("Request is not authorized. Check header token");
        } else if (!isHaveGameAccount(accountToken.getAccount(), gameId)) {
            throw new BadRequestException("Invalid game account");
        }

        return accountToken;
    }

    protected AccountToken authenticateTokenRequest(HttpServletRequest request) {
        AccountToken accountToken = getAccountTokenFromRequest(request);
        if (accountToken == null) {
            throw new UnauthorizedException("Request is not authorized. Check header token");
        }

        return accountToken;
    }

    private AccountToken getAccountTokenFromRequest(HttpServletRequest request) {
        AccountToken accountToken = null;
        String authorization = request.getHeader("Authorization");
        if (StringUtils.isNotEmpty(authorization)) {
            String[] authorizationData = authorization.split(":");
            if (authorizationData.length == 2) {
                accountToken = accountTokenService.findActiveToken(Long.parseLong(authorizationData[0]), authorizationData[1]);
            }
        }

        return accountToken;
    }

    protected void logAccountTokenErrorActivity(HttpServletRequest servletRequest, AccountTokenActivityTypeEnum activityType, AccountToken accountToken, String info) {
        if (accountToken != null) {
            accountTokenActivityTrackingService.saveAsync(AccountTokenActivityTracking.create(accountToken,
                    activityType, servletRequest.getRemoteAddr(), AccountActivityResultEnum.FAIL, servletRequest.getRequestURI(), info));

        }
    }


    protected void logAccountTokenSuccessActivity(HttpServletRequest servletRequest, AccountTokenActivityTypeEnum activityType, AccountToken accountToken, String info) {
        if (accountToken != null) {
            accountTokenActivityTrackingService.saveAsync(AccountTokenActivityTracking.create(accountToken,
                    activityType, servletRequest.getRemoteAddr(), AccountActivityResultEnum.SUCCESS, servletRequest.getRequestURI(), info));

        }
    }

    protected void logAccountErrorActivity(HttpServletRequest servletRequest, AccountActivityTypeEnum activityType, Account account, String info) {
        if (account != null) {
            accountActivityTrackingService.saveAsync(AccountActivityTracking.create(account, activityType, servletRequest.getRemoteAddr(), AccountActivityResultEnum.FAIL, servletRequest.getRequestURI(), info));

        }
    }

    protected void logAccountSuccessActivity(HttpServletRequest servletRequest, AccountActivityTypeEnum activityType, Account account, String info) {
        if (account != null) {
            accountActivityTrackingService.saveAsync(AccountActivityTracking.create(account, activityType, servletRequest.getRemoteAddr(), AccountActivityResultEnum.SUCCESS, servletRequest.getRequestURI(), info));

        }
    }

    protected boolean isHaveGameAccount(Account account, Long gameId) {
        boolean isHaveGameAccount = false;
        Set<Game> games = account.getGames();
        for (Game game : games) {
            if (Objects.equals(game.getId(), gameId)) {
                isHaveGameAccount = true;
                break;
            }
        }
        return isHaveGameAccount;
    }


    protected Long authenticateRequest(HttpServletRequest request, Long gameId) throws Exception {
        String remoteAddress = request.getRemoteAddr();
        String authorizationValue = request.getHeader("Authorization");
        Long accountId = WebUtils.getAccountIdFromAuthorizationValue(authorizationValue);
        if (StringUtils.isEmpty(authorizationValue)) {
            throw new UnauthorizedException("Request is not authorized. Check header token");
        }
        String cacheKey = String.format(AUTHENTICATE_REQUEST_CACHE_KEY, remoteAddress);
        String cacheValue = redisServer.get(cacheKey);

        boolean isValidCache = isValidCacheValue(cacheValue, gameId, authorizationValue);
        if (isValidCache) {
            return accountId;
        } else {
            validateGame(gameId);
            AccountToken accountToken = authenticateTokenRequest(request, gameId);

            logAccountTokenSuccessActivity(request, AccountTokenActivityTypeEnum.VALIDATE, accountToken, "Valid token");
            redisServer.add(cacheKey, getAuthorizationCacheValue(gameId, authorizationValue), AUTHENTICATE_REQUEST_CACHE_TIME);

            return accountToken.getAccount().getId();
        }
    }

    private boolean isValidCacheValue(String cacheValue, Long gameId, String authorizationValue) {
        return StringUtils.isNotEmpty(cacheValue) && Objects.equals(getAuthorizationCacheValue(gameId, authorizationValue), cacheValue);
    }

    private String getAuthorizationCacheValue(Long gameId, String authorizationValue) {
        return gameId + ":" + authorizationValue;
    }

}
