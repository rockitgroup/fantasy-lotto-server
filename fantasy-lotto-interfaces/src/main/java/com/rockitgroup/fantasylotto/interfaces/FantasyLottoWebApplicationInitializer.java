package com.rockitgroup.fantasylotto.interfaces;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoReactiveDataAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.scheduling.annotation.EnableAsync;

@Slf4j
@SpringBootApplication(exclude = {
        DataSourceAutoConfiguration.class,
        MongoDataAutoConfiguration.class,
        MongoReactiveDataAutoConfiguration.class
})
@EnableAsync
@ImportResource({
        "classpath:spring/beanconfig/${" + AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME + "}/application-web-context.xml"
})
public class FantasyLottoWebApplicationInitializer {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(FantasyLottoWebApplicationInitializer.class,args);
    }

}
