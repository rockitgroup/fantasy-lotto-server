package com.rockitgroup.fantasylotto.interfaces.controller.account;

import com.google.common.collect.Sets;
import com.rockitgroup.fantasylotto.domain.dto.account.AccountDTO;
import com.rockitgroup.fantasylotto.domain.dto.account.AccountTokenDTO;
import com.rockitgroup.fantasylotto.domain.dto.account.SocialLinkTypeDTO;
import com.rockitgroup.fantasylotto.domain.dto.account.request.*;
import com.rockitgroup.fantasylotto.domain.dto.game.AccountGameSettingDTO;
import com.rockitgroup.fantasylotto.domain.model.account.*;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccessIssueTypeEnum;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountActivityTypeEnum;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountTokenActivityTypeEnum;
import com.rockitgroup.fantasylotto.domain.model.enumeration.account.AccountStatusEnum;
import com.rockitgroup.fantasylotto.domain.model.game.AccountGameSetting;
import com.rockitgroup.fantasylotto.domain.model.game.Game;
import com.rockitgroup.fantasylotto.domain.service.account.*;
import com.rockitgroup.fantasylotto.domain.service.game.AccountGameSettingService;
import com.rockitgroup.fantasylotto.interfaces.controller.AppBaseController;
import com.rockitgroup.infrastructure.vitamin.common.dto.ResponseDTO;
import com.rockitgroup.infrastructure.vitamin.common.exception.BadRequestException;
import com.rockitgroup.infrastructure.vitamin.common.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Slf4j
@RestController
@RequestMapping(value = "/api/v1/account")
public class AccountController extends AppBaseController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountTokenService accountTokenService;

    @Autowired
    private AccountDeviceService accountDeviceService;

    @Autowired
    private AccountGameSettingService accountGameSettingService;

    @Autowired
    private AccountSocialLinkService accountSocialLinkService;

    @Autowired
    private SocialLinkTypeService socialLinkTypeService;

    @RequestMapping(value = "/login/email/confirm", method = RequestMethod.POST, consumes = {"application/json"})
    public ResponseDTO confirmLoginEmail(HttpServletRequest servletRequest, @RequestParam(value = "gameId", required = true) Long gameId, @RequestBody ConfirmLoginEmailRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();

        AccountActivityTypeEnum accountActivityType = AccountActivityTypeEnum.CONFIRM_LOGIN_EMAIL;
        Account account = null;
        try {
            requestDTO.validate();

            Game game = validateGame(gameId);

            account = accountService.findActiveAccountByEmail(requestDTO.getEmail());
            if (account == null) {
                throw new IllegalArgumentException("Account with this email does not exist");
            }

            AccountAccessIssue confirmIssue = accountService.confirmAccountAccessIssueEmailType(account, game, AccessIssueTypeEnum.LOGIN_EMAIL, requestDTO.getConfirmCode(), servletRequest.getRemoteAddr());
            if (confirmIssue != null) {
                accountTokenService.invalidateAllAccountTokens(account.getId());
                AccountToken accountToken = (AccountToken) accountTokenService.save(AccountToken.create(account));

                logAccountSuccessActivity(servletRequest, accountActivityType, account, "Confirm login email successfully");
                return generateSuccessResponse(responseDTO, accountToken, AccountTokenDTO.class);
            } else {
                throw new BadRequestException("Invalid or expired confirm code");
            }
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, account, accountActivityType, e);
        }
    }


    @RequestMapping(value = "/login/email", method = RequestMethod.POST, consumes = {"application/json"})
    public ResponseDTO requestLoginEmail(HttpServletRequest servletRequest, @RequestParam(value = "gameId", required = true) Long gameId, @RequestBody LoginEmailRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();

        AccountActivityTypeEnum accountActivityType = AccountActivityTypeEnum.LOGIN_EMAIL;
        Account account = null;
        try {
            requestDTO.validate();

            Game game = validateGame(gameId);

            account = accountService.findActiveAccountByEmail(requestDTO.getEmail());
            if (account == null) {
                throw new IllegalArgumentException("Account with this email does not exist");
            }

            accountService.startLoginEmailProcess(account, game, requestDTO.getEmail());

            logAccountSuccessActivity(servletRequest, accountActivityType, account, "Request Login email successfully");

            return generateSuccessResponse(responseDTO);
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO,  account, accountActivityType, e);
        }
    }

    @RequestMapping(value = "/update/email/confirm", method = RequestMethod.POST, consumes = {"application/json"})
    public ResponseDTO confirmUpdateAccountEmail(HttpServletRequest servletRequest, @RequestParam(value = "gameId", required = true) Long gameId, @RequestBody ConfirmUpdateAccountEmailRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();

        AccountTokenActivityTypeEnum accountTokenActivityType = AccountTokenActivityTypeEnum.CONFIRM_UPDATE_ACCOUNT_EMAIL;
        AccountToken accountToken = null;
        try {
            requestDTO.validate();

            Game game = validateGame(gameId);
            accountToken = authenticateTokenRequest(servletRequest, gameId);

            Account account = accountToken.getAccount();

            AccountAccessIssue confirmIssue = accountService.confirmAccountAccessIssueEmailType(account, game, AccessIssueTypeEnum.UPDATE_ACCOUNT_EMAIL, requestDTO.getConfirmCode(), servletRequest.getRemoteAddr());
            if (confirmIssue != null) {
                account.setEmail(confirmIssue.getConfirmContact());
                accountService.save(account);
                logAccountTokenSuccessActivity(servletRequest, accountTokenActivityType, accountToken, "Confirm update account email successfully");
                return generateSuccessResponse(responseDTO);
            } else {
                throw new BadRequestException("Invalid or expired confirm code");
            }
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, accountToken, accountTokenActivityType, e);
        }
    }

    @RequestMapping(value = "/update/email", method = RequestMethod.POST, consumes = {"application/json"})
    public ResponseDTO requestUpdateAccountEmail(HttpServletRequest servletRequest, @RequestParam(value = "gameId", required = true) Long gameId, @RequestBody UpdateAccountEmailRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();

        AccountTokenActivityTypeEnum accountTokenActivityType = AccountTokenActivityTypeEnum.UPDATE_ACCOUNT_EMAIL;
        AccountToken accountToken = null;
        try {
            requestDTO.validate();

            Game game = validateGame(gameId);
            accountToken = authenticateTokenRequest(servletRequest);

            Account existAccount = accountService.findActiveAccountByEmail(requestDTO.getEmail());
            if (existAccount != null && !Objects.equals(existAccount.getId(), accountToken.getAccount().getId())) {
                throw new IllegalArgumentException("Email already existed");
            }

            accountService.startUpdateAccountEmailProcess(accountToken.getAccount(), game, requestDTO.getEmail());

            logAccountTokenSuccessActivity(servletRequest, accountTokenActivityType, accountToken, "Request update account email successfully");

            return generateSuccessResponse(responseDTO);
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, accountToken, accountTokenActivityType, e);
        }
    }



    @RequestMapping(value = "/register-account-device", method = RequestMethod.POST, consumes = {"application/json"})
    public ResponseDTO registerAccountDevice(HttpServletRequest servletRequest, @RequestParam(value = "gameId", required = true) Long gameId, @RequestBody RegisterAccountDeviceRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();

        AccountActivityTypeEnum accountActivityType = AccountActivityTypeEnum.REGISTER_ACCOUNT_DEVICE;
        Account account = null;
        try {
            requestDTO.validate();

            Game game = validateGame(gameId);

            AccountDevice accountDevice = accountDeviceService.findOneByDeviceUniqueIdAndDeviceSecretKey(requestDTO.getDeviceUniqueId(), requestDTO.getDeviceSecretKey());
            if (accountDevice != null) {
                account = accountDevice.getAccount();
                if (!isHaveGameAccount(account, game.getId())) {
                    account.getGames().add(game);
                    accountService.save(account);
                }
                accountTokenService.invalidateAllAccountTokens(account.getId());
                logAccountSuccessActivity(servletRequest, accountActivityType, account, "Already registered account device");

            } else {
                account = (Account) accountService.save(Account.create(Sets.newHashSet(game)));

                accountDevice = AccountDevice.create(account, requestDTO.getDeviceUniqueId(), requestDTO.getDeviceSecretKey(), requestDTO.getDeviceOSVersion(), requestDTO.getDeviceModel(), servletRequest.getRemoteAddr());
                accountDeviceService.save(accountDevice);
                logAccountSuccessActivity(servletRequest, accountActivityType, account, "Register new account device");
            }

            AccountToken accountToken = (AccountToken) accountTokenService.save(AccountToken.create(account));
            return generateSuccessResponse(responseDTO, accountToken, AccountTokenDTO.class);
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, account, accountActivityType, e);
        }
    }

    @RequestMapping(value = "/validate", method = RequestMethod.POST, consumes = {"application/json"})
    public ResponseDTO validateAccount(HttpServletRequest servletRequest, @RequestParam(value = "gameId", required = true) Long gameId) {
        ResponseDTO responseDTO = new ResponseDTO();

        AccountTokenActivityTypeEnum accountTokenActivityType = AccountTokenActivityTypeEnum.VALIDATE;
        AccountToken accountToken = null;
        try {

            validateGame(gameId);

            accountToken = authenticateTokenRequest(servletRequest, gameId);

            logAccountTokenSuccessActivity(servletRequest, accountTokenActivityType, accountToken, "Valid token");

            return generateSuccessResponse(responseDTO);
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, accountToken, accountTokenActivityType, e);
        }
    }

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public ResponseDTO getAccountInfo(HttpServletRequest servletRequest, @RequestParam(value = "gameId", required = true) Long gameId) {
        ResponseDTO responseDTO = new ResponseDTO();
        AccountTokenActivityTypeEnum accountTokenActivityType = AccountTokenActivityTypeEnum.GET_ACCOUNT_INFO;

        AccountToken accountToken = null;
        try {
            Game game = validateGame(gameId);
            accountToken = authenticateTokenRequest(servletRequest, gameId);
            Account account = accountToken.getAccount();
            account.setGames(Sets.newHashSet(game));

            responseDTO = generateSuccessResponse(responseDTO, account, AccountDTO.class);
            logAccountTokenSuccessActivity(servletRequest, accountTokenActivityType, accountToken, "Get account info successfully");
            return responseDTO;
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, accountToken, accountTokenActivityType, e);
        }
    }

    @RequestMapping(value = "/game-settings", method = RequestMethod.POST, consumes = {"application/json"})
    public ResponseDTO createOrUpdateAccountGameSettings(HttpServletRequest servletRequest, @RequestParam(value = "gameId") Long gameId, @RequestBody CreateAccountGameSettingsRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();

        AccountTokenActivityTypeEnum accountTokenActivityType = AccountTokenActivityTypeEnum.CREATE_ACCOUNT_GAME_SETTINGS;
        AccountToken accountToken = null;
        try {
            requestDTO.validate();

            Game game = validateGame(gameId);
            accountToken = authenticateTokenRequest(servletRequest);

            Account account = accountToken.getAccount();

            Map<String, AccountGameSetting> existSettings = new HashMap<>();
            for (AccountGameSetting accountGameSetting : account.getGameSettings()) {
                existSettings.put(accountGameSetting.getSettingKey(), accountGameSetting);
            }

            List<AccountGameSetting> saveSettings = new ArrayList<>();
            for (AccountGameSettingDTO gameSettingDTO : requestDTO.getGameSettings()) {
                AccountGameSetting accountGameSetting = AccountGameSetting.create(account, game, gameSettingDTO.getSettingKey(), gameSettingDTO.getSettingValue());
                if (existSettings.containsKey(gameSettingDTO.getSettingKey())) {
                    accountGameSettingService.deleteDuplicateSettingKey(account.getId(), game.getId(), gameSettingDTO.getSettingKey());
                }
                saveSettings.add(accountGameSetting);
            }

            accountGameSettingService.save(saveSettings);

            logAccountTokenSuccessActivity(servletRequest, accountTokenActivityType, accountToken, "Create account settings successfully");
            return generateSuccessResponse(responseDTO);
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, accountToken, accountTokenActivityType, e);
        }
    }


    @RequestMapping(value = "/link-social", method = RequestMethod.POST, consumes = {"application/json"})
    public ResponseDTO linkSocialAccount(HttpServletRequest servletRequest, @RequestParam(value = "gameId", required = true) Long gameId, @RequestBody LinkSocialRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();

        AccountTokenActivityTypeEnum accountTokenActivityType = AccountTokenActivityTypeEnum.LINK_SOCIAL_ACCOUNT;
        AccountToken accountToken = null;
        try {
            requestDTO.validate();

            Game game = validateGame(gameId);

            accountToken = authenticateTokenRequest(servletRequest);

            SocialLinkType socialLinkType = socialLinkTypeService.findOneByTypeKey(requestDTO.getSocialLinkType());
            if (socialLinkType == null) {
                throw new NotFoundException(String.format("Social link type %s is not valid", requestDTO.getSocialLinkType()));
            }

            Account account = accountToken.getAccount();

            AccountSocialLink accountSocialLink = new AccountSocialLink();
            accountSocialLink.setGame(game);
            accountSocialLink.setAccount(account);
            accountSocialLink.setSocialAccountId(requestDTO.getSocialAccountId());
            accountSocialLink.setSocialAccountToken(requestDTO.getSocialAccountToken());
            accountSocialLink.setStatus(AccountStatusEnum.ACTIVE);
            accountSocialLink.setSocialLinkType(socialLinkType);
            accountSocialLink.setLinkedAt(DateTime.now());

            accountSocialLinkService.save(accountSocialLink);

            String logInfo = String.format("Link social account type %s of id %s and token %s successfully", requestDTO.getSocialLinkType(), requestDTO.getSocialAccountId(), requestDTO.getSocialAccountToken());
            logAccountTokenSuccessActivity(servletRequest, accountTokenActivityType, accountToken, logInfo);
            return generateSuccessResponse(responseDTO);
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, accountToken, accountTokenActivityType, e);
        }
    }


    @RequestMapping(value = "/social-link-type/list", method = RequestMethod.GET)
    public ResponseDTO listAllSocialLinkType(HttpServletRequest servletRequest) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            List<SocialLinkType> socialLinkTypes = socialLinkTypeService.listAll();
            return generateSuccessResponse(responseDTO, socialLinkTypes, SocialLinkTypeDTO.class);
        } catch (Exception e) {
            return generateFailResponse(servletRequest, responseDTO, e);
        }
    }
}
