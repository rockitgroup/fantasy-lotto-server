
INPUT_FOLDER_PATH = "superbowl-server/src/main/java/com/rockitgroup/superbowl/server/network/"
OUTPUT_FOLDER_PATH = "superbowl-server/build/python/generated/"

INPUT_FILE_NAME = "CommandType.java"
OUTPUT_FILE_NAME = "CommandType.cs"

JAVA_CONSTANT_WORDS = "public static final byte"
JAVA_CLASS_WORDS = "public class"

CSHARP_CONSTANT_WORDS = "public const sbyte"
CSHARP_CLASS_WORDS = "public class"

CHARACTERS = ["{","}"]

import errno
import os

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def main():
    mkdir_p(OUTPUT_FOLDER_PATH)

    f1 = open(INPUT_FOLDER_PATH + INPUT_FILE_NAME, "r")
    f2 = open(OUTPUT_FOLDER_PATH + OUTPUT_FILE_NAME, "w")

    for line in f1:
        if JAVA_CLASS_WORDS in line:
            f2.write(line.replace(JAVA_CLASS_WORDS, CSHARP_CLASS_WORDS))
        elif JAVA_CONSTANT_WORDS in line:
            f2.write(line.replace(JAVA_CONSTANT_WORDS, CSHARP_CONSTANT_WORDS))
        elif line[0] in CHARACTERS:
            f2.write(line)

    f1.close()
    f2.close()

if __name__ == '__main__':
    main()
