DROP TABLE IF EXISTS `batch_job_trackings`;
CREATE TABLE `batch_job_trackings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `jobName` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `result` varchar(50) DEFAULT NULL COMMENT 'Success or Fail',
  `output` text,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `batch_job_configs`;
CREATE TABLE `batch_job_configs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` varchar(50) NOT NULL,
  `className` varchar(300) NOT NULL,
  `jobName` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active flag',
  `schedule` varchar(50) NOT NULL,
  `group` varchar(50) NOT NULL,
  `trigger` varchar(50) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `crawling_events`;
CREATE TABLE `crawling_events` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `crawlStartedAt` datetime NOT NULL,
  `crawlEndedAt` datetime NOT NULL,
  `crawlUrl` varchar(3000) NOT NULL,
  `status` varchar(50) NOT NULL,
  `result` varchar(50) DEFAULT NULL COMMENT 'Success or Fail',
  `crawlType` varchar(100) NOT NULL COMMENT 'App, trend, deals, etc.',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;