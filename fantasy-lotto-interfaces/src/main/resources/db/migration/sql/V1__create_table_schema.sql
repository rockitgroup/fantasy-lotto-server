DROP TABLE IF EXISTS `lotto_configs`;
CREATE TABLE `lotto_configs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `configKey` varchar(100) NOT NULL,
  `configValue` varchar(100) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lotto_bets`;
CREATE TABLE `lotto_bets` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lottoGameId` bigint(20) NOT NULL,
  `betById` bigint(20) NOT NULL,
  `betModeId` bigint(20) NOT NULL,
  `betDrawEventId` bigint(20) NOT NULL,
  `betAmount` double NOT NULL,
  `betNumber` int(10) NOT NULL,
  `betAt` datetime NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'ACTIVE',
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lotto_bet_balls`;
CREATE TABLE `lotto_bet_balls` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lottoBetId` bigint(20) NOT NULL,
  `cover` tinyint(1) NOT NULL DEFAULT 0,
  `position` int(10) NOT NULL,
  `value` int(10) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lotto_bet_modes`;
CREATE TABLE `lotto_bet_modes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `winningRate` int(10) NOT NULL,
  `totalCoverBallAllow` int(10) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lotto_bet_winners`;
CREATE TABLE `lotto_bet_winners` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `resultId` bigint(20) NOT NULL,
  `lottoBetId` bigint(20) NOT NULL,
  `winningAmount` double NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'PENDING',
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lotto_games`;
CREATE TABLE `lotto_games` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `key` varchar(100) NOT NULL,
  `iconImageUrl` varchar(3000) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `totalNormalBall` int(10) NOT NULL,
  `normalBallMin` int(10) NOT NULL,
  `normalBallMax` int(10) NOT NULL,
  `haveSpecialBall` tinyint(1) NOT NULL,
  `specialBallMin` int(10) NOT NULL,
  `specialBallMax` int(10) NOT NULL,
  `latestResultId` bigint(20) DEFAULT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'ACTIVE',
  `nextDrawEventId` bigint(20) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lotto_game_prizes`;
CREATE TABLE `lotto_game_prizes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `lottoGameId` bigint(20) NOT NULL,
  `prizeAmount` double NOT NULL,
  `numNormalWinningBall` int(10) NOT NULL,
  `winningSpecialBall` tinyint(1) NOT NULL,
  `grandPrize` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lotto_game_results`;
CREATE TABLE `lotto_game_results` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lottoGameId` bigint(20) NOT NULL,
  `drawEventId` bigint(20) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lotto_ball_results`;
CREATE TABLE `lotto_ball_results` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gameResultId` bigint(20) NOT NULL,
  `position` int(10) NOT NULL,
  `value` int(10) NOT NULL,
  `specialBall` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lotto_draw_schedules`;
CREATE TABLE `lotto_draw_schedules` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lottoGameId` bigint(20) NOT NULL,
  `dayOfWeek` varchar(30) NOT NULL,
  `hour` int(10) NOT NULL,
  `minute` int(10) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lotto_draw_events`;
CREATE TABLE `lotto_draw_events` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lottoGameId` bigint(20) NOT NULL,
  `drawAt` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lotto_tickets`;
CREATE TABLE `lotto_tickets` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `purchaseRequestId` bigint(20) NOT NULL,
  `lottoGameId` bigint(20) NOT NULL,
  `price` double NOT NULL,
  `scannedTicketImageUrl` varchar(3000) default null,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lotto_ticket_balls`;
CREATE TABLE `lotto_ticket_balls` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lottoTicketId` bigint(20) NOT NULL,
  `position` int(10) NOT NULL,
  `value` int(10) NOT NULL,
  `specialBall` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `lotto_ticket_purchase_requests`;
CREATE TABLE `lotto_ticket_purchase_requests` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `requestById` bigint(20) NOT NULL,
  `purchaseAmount` DOUBLE NOT NULL,
  `feeAmount` DOUBLE NOT NULL,
  `totalFinalAmount` DOUBLE NOT NULL,
  `requestAt` datetime NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'PENDING',
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `lotto_ticket_winners`;
CREATE TABLE `lotto_ticket_winners` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ticketId` bigint(20) NOT NULL,
  `resultId` bigint(20) NOT NULL,
  `winningAmount` DOUBLE NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'PENDING',
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `origin_sites`;
CREATE TABLE `origin_sites` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `siteKey` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `crawling` tinyint(1) NOT NULL,
  `siteType` varchar(100) NOT NULL COMMENT 'App, trend, deals, etc.',
  `homeUrl` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `origin_sites_unique01` (`name`),
  UNIQUE KEY `origin_sites_unique02` (`siteKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `origin_site_urls`;
CREATE TABLE `origin_site_urls` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `originSiteId` bigint(20) NOT NULL,
  `url` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `origin_sites_urls_unique01` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
